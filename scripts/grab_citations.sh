#!/bin/bash
# First input is the .tex file and second input is bibligraphy file, outputs a subset of the bibliography matching the .tex file
cat $1 | grep -o  '\cite{\w\+\(,\(\s\)\?\w\+\)*}' | grep -o '\w\+\(,\(\s\)\?\w\+\)*' \
	| tr , '\n' | tr -d ' ' | sed '/cite/d' \
	| sort | uniq \
	| xargs -ix awk '/^@.*{x,/,/^}$/'  $2
