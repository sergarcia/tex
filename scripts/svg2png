#!/usr/bin/bash
# See display_usage()

shopt -s extglob

display_usage() {
	printf "Bulk conversion of svg (or svgz) files to png\n"
	printf "\t- Optional argument: -p or --path,  is the path to directory with svg files. Default is current directory.\n"
	printf "\t- Optional argument: -t or --type, conversion type options are png and pdf, default is png. In general png is preferable\n"
	printf "\t- Optional argument: -d or --dpi, is the dpi (for png), default is 300.\n"
	printf "# Examples:\n"
	printf "\tsvg2png\n"
	printf "\tsvg2png -d=200\n"
	printf "\tsvg2png -p=/home/user/here -d=200\n"
}

# -------- Argument parsing --------

if [[ ( ${1} == "--help") ||  ${1} == "-h" ]]
then
	display_usage
	exit 1
fi

SEARCHPATH="./"
DPI="300"
TYPE="png"

for i in "$@"
do
	case $i in
		-d=*|--dpi=*)
			DPI="${i#*=}"
			shift # past argument=value
			;;
		-p=*|--searchpath=*)
			SEARCHPATH="${i#*=}"
			shift # past argument=value
			;;
		-t=*|--type=*)
			TYPE="${i#*=}"
			shift # past argument=value
			;;
		*)
			printf "Error unkown option \"%s \"\n" "$i"
			display_usage
			exit 1
			;;
	esac
done

# -------- Main program --------

echo "directory: ${SEARCHPATH}"
echo "dpi: $DPI"

for fullfile in "${SEARCHPATH}"*.svg?(z); do
	sourcefile="$fullfile"
	filename=$(basename -- "$fullfile")
	filename="${filename%.*}"
	case "$TYPE" in
		png)
			newfile="${SEARCHPATH}$filename.png"
			inkscape --without-gui --export-area-page --export-dpi="$DPI" --file="$sourcefile" --export-png="$newfile" &> /dev/null
			;;
		pdf)
			newfile="${SEARCHPATH}$filename.pdf"
			inkscape --without-gui --export-area-page --export-dpi="$DPI" --file="$sourcefile" --export-pdf="$newfile" &> /dev/null
			;;
		*)
			printf "Error unkown output type \"%s \"\n" "$TYPE"
			display_usage
			exit 1
			;;
	esac
	echo "Source: ${sourcefile}
	Writen to: ${newfile}"
done
