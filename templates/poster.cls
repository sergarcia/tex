% Author: Sergio Garcia
%
% # General description:
% 	This is inspired a0 poster class. But that is pretty outdated and it easily breaks when one tries to change even simple details like poster size. Instead, the article class is used here and some simple modifications are made around it. One of the advantages of this approach is that you do not have to worry about the layout fitting the page and alignment as much as with packages like beamerposter.
%
% # Design goals:
% 	- Minimalist look (avoid boxes, background colors, and uneven arrangements)
% 	- Automate alignment and content arrangement as much as possible
% 	- Relatively small poster size to limit content (what % of your poster do you usually go through when you present it to someone? Are there sections that you rarely or never cover?) and avoid issues while hanging the poster in small environments. (See more on notes about size)
% 	- Landscape poster, since in portrait posters the bottom is almost always unreadable.
%
% # Notes:
% 	- The poster header might be better abstracted as a custom environment. But for now it seems that the flexibility it provides to avoid this might be useful.
%   - Currently I am using \footnotesize heavily throughout the poster rather than normalsize. So the font sizes need adjustment.
%   - Font size in figures are much smaller than font sizes in text (even when using footnote size). I think this is a consequence of having too many columns, so perhaps the optimal number of columns is 3.
%   - Also the space between columns it is actually a bit too wide. So reducing it would also be helpful.
%   - Overall, for 36x27 the number of columns needs to be reduced to 3 and the size of the title should only ocuppy about 20% or less of the size of the poster (regardless of length, so title should be placed in a fixed size environemnt perhaps \resizebox?).

%

%
% # Comments about size:
% 	The largest size in my opinion is 48x36 (also known as Arch E or Arch 6), which produces a width/height ratio of 1.33. Using this ratio as a reference, and considering that poster printing is often constrained in the size of one dimension (e.g. to 24 -Arch C-, 36 -Arch D-, and 42 -Arch E1-) we can reduce our poster to:
% 	- 42x30 (Arch E1)
% 	- 36x27 (closest to Arch D which is 36x24)
%
% 	Note that while Arch is common in the US, A0 is common in Europe.
% 	A0 in inches is 33.11 x 46.81
% 	A0 is rather narrow and elongated, which is not practical (who could have thought that creating a metric system based purely on symmetry considerations would lead to impractical units?)
% 	So the poster has to fit on a portrait A0 board I suggest to go for the  36x27 (which will 3 extran inches or 7 cm, that should fit).
%
% ----------------------------------------------------


\ProvidesClass{poster}[Poster class]
\LoadClass{article}
%%%%%%%%%%%%%%
% Font sizes %
%%%%%%%%%%%%%%
\renewcommand{\tiny}{\fontsize{12}{14}\selectfont}
\renewcommand{\scriptsize}{\fontsize{14.4}{18}\selectfont}
\renewcommand{\footnotesize}{\fontsize{17.28}{22}\selectfont}
\renewcommand{\small}{\fontsize{20.74}{25}\selectfont}
\renewcommand{\normalsize}{\fontsize{17.28}{22}\selectfont} % footnotesize
\newcommand{\realnormalsize}{\fontsize{24.88}{30}\selectfont}
\renewcommand{\large}{\fontsize{29.86}{37}\selectfont}
\renewcommand{\Large}{\fontsize{35.83}{45}\selectfont}
\renewcommand{\LARGE}{\fontsize{43}{54}\selectfont}
\renewcommand{\huge}{\fontsize{51.6}{64}\selectfont}
\renewcommand{\Huge}{\fontsize{61.92}{77}\selectfont}
\newcommand{\veryHuge}{\fontsize{74.3}{93}\selectfont}
\newcommand{\VeryHuge}{\fontsize{89.16}{112}\selectfont}
\newcommand{\VERYHuge}{\fontsize{107}{134}\selectfont}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% My color palette (seaborn deep and pastel) %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{sblue}{RGB}{76, 114, 176}       %
\definecolor{sorange}{RGB}{221, 132, 82}     %
\definecolor{sgreen}{RGB}{85, 168, 104}      %
\definecolor{sred}{RGB}{196, 78, 82}         %
\definecolor{spurple}{RGB}{129, 114, 179}    %
\definecolor{sbrown}{RGB}{147, 120, 96}      %
\definecolor{spink}{RGB}{218, 139, 195}      %
\definecolor{sgray}{RGB}{140, 140, 140}      %
\definecolor{syellow}{RGB}{204, 185, 116}    %
\definecolor{scyan}{RGB}{100, 181, 205}      %
\definecolor{slblue}{RGB}{161, 201, 244}     %
\definecolor{slorange}{RGB}{255, 180, 130}   %
\definecolor{slgreen}{RGB}{141, 229, 161}    %
\definecolor{slred}{RGB}{255, 159, 155}      %
\definecolor{slpurple}{RGB}{208, 187, 255}   %
\definecolor{slbrown}{RGB}{222, 187, 155}    %
\definecolor{slpink}{RGB}{250, 176, 228}     %
\definecolor{slgray}{RGB}{207, 207, 207}     %
\definecolor{slyellow}{RGB}{235, 234, 21}    %
\definecolor{slcyan}{RGB}{185, 242, 240}     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% General Packages %
%%%%%%%%%%%%%%%%%%%%
\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster

\usepackage{times} % Use the times font
%\usepackage{palatino} % Uncomment for alternative font

\pagenumbering{gobble} % no page number

%Figures
\usepackage{graphicx} % Required for including images
\usepackage{float} % Force figure placement
%\usepackage{booktabs} % Top and bottom rules for table
%\usepackage[font=small,labelfont=bf]{caption} % Required for specifying captions to tables and figures
%Tables
\usepackage{wrapfig} % Allows wrapping text around tables and figures %TODO is it needed?
\usepackage{array}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}} % Centers
\newcolumntype{M}[1]{>{\centering\arraybackslash}m{#1}} % Centers vertically


% Tables
\usepackage{booktabs} % Professional table formatting
\usepackage[table,svgnames]{xcolor} % Row colors
\usepackage{multirow} % Must be loaded before xcolor

%Math
\usepackage{amsfonts, amsmath, amsthm, amssymb, mathtools, cases} % For math fonts, symbols and environments
%\allowdisplaybreaks % allows page breaks inside equations. Use \begin{displaybreak} for better control

% Bibliography
\usepackage[
    backend=biber,
    style=nature,
    sorting=none,
    doi=false,
    url=false,
    isbn=false,
    date=year,
    maxnames=2,
]{biblatex}
\renewcommand*{\bibfont}{\tiny}

% Figure caption color
\usepackage{caption}
\usepackage[font={color=sgreen,scriptsize},figurename=Fig.,labelfont={it}]{caption}

% Title/subtitle colors
\usepackage{titlesec}
\titleformat{\section}
{\color{sblue}\normalfont\Large\bfseries}
{\color{sblue}\thesection}{1em}{}
\titleformat{\subsection}
{\color{sorange}\normalfont\realnormalsize\bfseries}
{\color{sorange}\thesection}{1em}{}

% Footer
\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\lhead{}
\rhead{}
\fancyfootoffset[LO]{-1cm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Custom environments and commands %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\makeatletter
\renewenvironment{abstract}{%
	\centerline{\textcolor{sblue}{\bfseries \Large\abstractname}}
      	\quotation
	\small
	}
\makeatother

\newcommand{\ptitle}[1]{
	\veryHuge \color{sblue} \textbf{#1} \color{Black}\\
}

\newcommand{\pauthors}[1]{
	\huge \textbf{#1}\\[0.5cm]
}

\newcommand{\paffiliations}[1]{
\realnormalsize #1 \\
}

\newcommand{\pcontact}[1]{
\Large \texttt{#1} \\
}

