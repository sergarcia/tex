
## Scripts
These are general purpose programs to quickly accomplish common document preparation tasks involving latex or markdown.

- `svg2png` Converts all svg files in a given directory to png. Requires Inkscape.
- `x2pdf` Converts documents (e.g., Word) and slides (e.g., Powerpoint) to pdf. Requires Libreoffice and pandoc.
- `grab_citations.sh` Creates a bibtex file that only contains the references used in a given manuscript (bibtool can also do this, see Notes below).
- `find_abbrv.sh` Returns an alphabetically sorted list of unique abbreviations. The abbreviations match a pattern of two or more uppercase letters which may be followed by lowercase letters and numbers. Useful to find abbreviations in your paper quickly and without mistakes.
- `beamerdraft.py` Automatically generates a beamer presentation from a paper. Preserves the section structure and includes each figure in a frame.


## System
These are scripts that may require some adjustments based on your system (e.g.,
declare a certain environment variable)

- `bib-add` adds a bibtext citation from the clipboard to your bibliography file.
- `sent2pdf.sh` converts a [suckless sent](https://tools.suckless.org/sent/) presentation to a pdf of the specified screen size.

## Templates

- `paper.sty` A template to prepare complete manuscript drafts, including main and
supplementary files. Does not use macros.
- `presentation.sty` An aesthetically simple presentation beamer template that automatically generates a table of
contents to transition between sections. It is also effective to create slide handouts (e.g., multiple slides per page without transition effects) including notes, and research reports with a sidebar for sections.
- `poster.sty` A poster template that seeks to automate away trivial aspects of content arrangement. The begining of the file also includes discussion about poster design.

All my LaTeX templates use the same [Seaborn-based](https://seaborn.pydata.org/tutorial/color_palettes.html) color palette that I also use to generate figures. This creates beautiful visual consistency and simplifies color selection.

## Notes

- For general bibtex file manipulation (sorting, merging, etc) use [bibtool](https://github.com/ge-ne/bibtool).For example, `bibtool -s -d bibliography1.bib bibliography2.bib` merges two bib files and comments out duplicate entries. For specific shell scripts to accomplish similar tasks also look at [bibtools](https://www.ctan.org/pkg/bibtools)
- Certain files do not use an extension in the name. I currently use this notation for programs are tools that may be used rather regularly, which shortens typing the name in the terminal.
- For minimalist presentations check out [sent](https://tools.suckless.org/sent/).

